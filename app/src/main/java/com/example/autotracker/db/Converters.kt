package com.example.autotracker.db

import androidx.room.TypeConverter
import java.math.BigDecimal

class Converters {
    @TypeConverter
    fun bigDecimalToString(value: BigDecimal?): String? {
        return value?.toString()
    }

    @TypeConverter
    fun bigDecimalFromString(value: String?): BigDecimal? {
        return value?.let { BigDecimal(it) }
    }
}